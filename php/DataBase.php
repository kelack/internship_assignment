<?php
class DataBase {
    private $user; 
    public function __construct($userValues,$file_name)
    {   
        require 'User.php';

        // //Создание из файла $file_name массивa
        $file_name = '../json/'.$file_name;
        $arrayJson = json_decode(file_get_contents($file_name),true);

        $this->$user = new DataBase($userValues,$arrayJson);
    }

    public function Create()
    {
        $user_Create = $this->user;

        //Вызываем метод, для проверки полей и записываем в $examination
        $examination = $user_Create->Checking_Fields_In_Function_Create();

        //если в $examination есть строка с ошибкой, возвращаем ошибку
        if(is_string($examination))
            return $examination;
        else
        //если в $examination нет ошибки, создаем массив из значений введенных пользователем
        //добавляем массив в базу данных и возвращаем имя пользователя
            return $user_Create->Convert_Array_And_Write_To_File($user_Create->Creating_An_Array());

    }

    public function Read()
    {
        $user_Create = $this->user;

        return $user_Create->Checking_Fields_In_Function_Read();
    }
}
?>